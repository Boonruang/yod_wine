/**
 * Created by aekachaiboonruang on 7/27/17.
 */
import Caller from '../shared/caller'
const caller = Caller()
export default{
  list: () => caller.get('cms/user'),
  create: (offerId, message) => caller.post(`offer/${offerId}/detail`, {detail: message}),
  update: (offerId, noteId, message) => caller.put(`offer/${offerId}/detail/${noteId}`, {detail: message}),
  delete: (offerId, noteId) => caller.delete(`offer/${offerId}/detail/${noteId}`)
}
