import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'

// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'
import Login from '../components/GeneralViews/login.vue'

// Admin pages
import Admin from 'src/components/Dashboard/Views/Overview.vue'
import Account from 'src/components/Dashboard/Views/UserProfile.vue'
// import Notifications from 'src/components/Dashboard/Views/Notifications.vue'
// import Icons from 'src/components/Dashboard/Views/Icons.vue'
// import Maps from 'src/components/Dashboard/Views/Maps.vue'
import Passcode from 'src/components/Dashboard/Views/Typography.vue'
import Qrcode from 'src/components/Dashboard/Views/TableList.vue'

const routes = [
  { path: '/login', component: Login },
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/admin/overview'
  },
  {
    path: '/admin',
    component: DashboardLayout,
    redirect: '/admin/stats',
    children: [
      {
        path: 'overview',
        name: 'ADMIN',
        component: Admin
      },
      {
        path: 'stats',
        name: 'ACCOUNT',
        component: Account
      },
      {
        path: 'typography',
        name: 'PASS CODE',
        component: Passcode
      },
      {
        path: 'table-list',
        name: 'QR CODE',
        component: Qrcode
      }
    ]
  },
  { path: '*', component: NotFound }
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
